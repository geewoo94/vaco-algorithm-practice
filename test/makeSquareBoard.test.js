import { expect } from 'chai';
import makeSquareBoard from '../lib/makeSquareBoard';

describe('make square board', function() {
  it('should pass base case', function(done) {
    expect(makeSquareBoard(3)).to.deep.equal([['*', '*', '*'],
                                              ['*', '-', '*'],
                                              ['*', '*', '*']]);
    expect(makeSquareBoard(5)).to.deep.equal([['*', '*', '*', '*', '*'],
                                              ['*', '-', '-', '-', '*'],
                                              ['*', '-', '*', '-', '*'],
                                              ['*', '-', '-', '-', '*'],
                                              ['*', '*', '*', '*', '*']]);
    done();
  })
})