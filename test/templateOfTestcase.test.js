import { expect } from 'chai';
import testFunction from '../lib/templateOfMainFuntion';

describe.skip('test testFunction', function () {
  it('should pass base cases', function (done) {
    expect(testFunction(7)).to.eql([2, 5]);
    expect(testFunction(26)).to.eql([13, 13]);
    expect(testFunction(11)).to.eql(undefined);
    expect(testFunction(NaN)).to.eql(undefined);
    expect(testFunction([])).to.eql(undefined);
    expect(testFunction(Infinity)).to.eql(undefined);
    expect(testFunction(-Infinity)).to.eql(undefined);
    expect(testFunction(1000000)).to.eql([499943, 500057]);
    done();
  });
});
