import { expect } from 'chai';
import testFunction from '../lib/01_convertNumber';

describe.skip('test testFunction', function() {
  it('should pass base cases', function(done) {
    expect(testFunction(10, 10)).to.eql(10);
    expect(testFunction(3, 2)).to.eql(11);
    expect(testFunction(5, 3)).to.eql(12);
    expect(testFunction(0, 4)).to.eql(0);
    // some more tests..
    done();
  });
});
