//  participant    	                          completion	                         return
//  [leo, kiki, eden]	                        [eden, kiki]	                       leo
//  [marina, josipa, nikola, vinko, filipa]	  [josipa, filipa, marina, nikola]	   vinko
//  [mislav, stanko, mislav, ana]	            [stanko, ana, mislav]	               mislav

import { expect } from 'chai';
import marathoners from '../lib/justin-marathoners.js';

describe('test testFunction', function () {
  it('should pass base cases', function (done) {
    expect(marathoners(['leo', 'kiki', 'eden'],['eden', 'kiki'])).to.eql('leo');
    expect(marathoners(['marina', 'josipa', 'nikola', 'vinko', 'filipa'], ['josipa', 'filipa', 'marina', 'nikola'])).to.eql('vinko');
    expect(marathoners(['mislav', 'stanko', 'mislav', 'ana'], ['stanko', 'ana', 'mislav'])).to.eql('mislav');
    done();
  });
});
