/**
 *
 * ####### 테스트 문제 #######
 *
 * # number의 범위는 -1000000 이상 1000000 이하이다.
 *   ex: -1000000 <= number <= 1000000
 *
 * # number가 두 소수의 합 (a + b)로 표현될수 있다면
 *   [a, b]; (a <= b) 배열 형태로 반환하라
 *   ex: 7 = [2, 5], [5, 2] => return [2, 5];
 *
 * # 정답이 여러개라면 a * b의 값이 가장 큰 값을 반환한다.
 *   ex: 26 = [3, 23], [7, 19], [13, 13] => return [13, 13];
 *
 * # 표현될수 없거나 number의 자료형이 숫자가 아니라면 undefined를 반환한다.
 *
 */

export default function templateOfMainFunction(number) {
  if(Number.isNaN(number) || number === Infinity) return undefined;

  const initNumbers = [];
  for(let i = 0 ; i <= number ; i++) {
    initNumbers.push(i);
  }

  const primes = [];
  for(let i = 2 ; i <= number ; i++) {
    if(initNumbers[i] !== null) {
      primes.push(i);

      let j = i;
      while (j <= number) {
        j += i;
        initNumbers[j] = null;
      }
    }
  }

  const result = [];
  for(let j = primes.length - 1 ; j >= primes.length/2 ; j--) {
    let i = 0;
    while(primes[i] + primes[j] <= number) {
      if(primes[i] + primes[j] === number) {
        result.push([primes[i], primes[j]]);
      }
      i++;
    }
  }

  result.sort((a, b) => {
    return (b[0] * b[1]) - (a[0] * a[1]);
  })

  return (result[0]) ? result[0] : undefined;
}
