초기 설정
-------------

####지정된 파일 외엔 수정하면 안됩니다.

- 먼저 `npm install`을 실행해 줍니다.
- 자신만의 브랜치를 만듭니다.

두번째 부터
---------

- `git pull`을 이용해 업데이트 해줍니다.

공통
---

- `/lib` 폴더에 `branch name + *.js`파일을 만들어줍니다.
- `/test` 폴더에 `branch name + *.test.js`파일을 만들어줍니다.
- 문제를 만들어 줍니다.
- 모두 만들었다면 `gitlab`에서 `merge request`를 해줍니다.
- 서로가 만든 문제를 풀고 각자 풀이 방법을 공유합니다.

## Library

  1. [npm-watch 사용법](#npm-watch)
  2. [*.test.js 명령어](#*.test.js)
  3. [readme.md 명령어](#readme.md)

## Algorithm

  0. [templateOfMainFuntion](#templateOfMainFuntion)

## npm-watch
- 파일의 수정상황을 감지하는 라이브러리 입니다.
- 현재는 수정 상황이 감지되면 `npm test`가 자동으로 실행됩니다.

- 명령어 `npm run watch`
- 종료법 `Ctrl + C`

- 선택한 파일만 감지하는법
- 해당 폴더의 package.json을 찾아 들어가서
  src 부분에 원하는 파일이나 폴더의 경로를 지정해줍니다.

  ```javascript
  "watch": {
    "test": "./{src}"
  },
  ```

  현재 기본값은 전체 파일 감시입니다.

Link: [npm-watch][npm-watch-link]

[npm-watch-link]: https://www.npmjs.com/package/npm-watch "go npm-watch"

## *.test.js

Link: [mocha][mocha-link]

[mocha-link]: https://mochajs.org/ "Go mocha"

## readme.md

Link: [readme 명령어 github][readme-link]

[readme-link]: https://gist.github.com/ihoneymon/652be052a0727ad59601 "Go readme"

## templateOfMainFuntion

- dosomething...